$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "zll15/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "zll15"
  s.version     = Zll15::VERSION
  s.authors     = ["Helmut Hissen"]
  s.email       = ["helmut@zeebar.com"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of Zll15."
  s.description = "TODO: Description of Zll15."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.4"
  s.add_dependency "rspec-rails", "~> 3.3"
  s.add_dependency "thor", "~> 0.19"
  s.add_dependency "pdf-reader"

  s.add_development_dependency "guard", "~> 2.13"
  s.add_development_dependency "guard-rspec", "~> 4.6"
  s.add_development_dependency "hashdiff", "~> 0.2"
  s.add_development_dependency "sqlite3"

end
