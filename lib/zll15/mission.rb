
module Zll15

  class Misson

    attr_reader :mission_no, :title, :summary, :basic_description, :requirement, :values, :leniency, :method_constraint

    def initialize( mission_no, title, summary, basic_description, requirement, values, leniency, method_constraint )
      @mission_no = mission_no
      @title = title
      @summary = summary
      @basic_description = basic_description
      @requirement = requirement
      @values = values
      @leniency = leniency
      @method_constraint = method_constraint
    end

  end

end

