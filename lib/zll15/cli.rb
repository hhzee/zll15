
require 'thor'
require 'open-uri'
require 'zll15'


module Zll15

  class CLI < Thor


    desc "version", "show version of zll15"

    def version()
      puts Zll15::VERSION
    end


    desc "config", "get or set the russell configuration"

    def config(name = nil, value = nil)
      challenge = Zll15::Challenge.new()
      if name.nil?
        puts challenge.config.config_hash.to_yaml
      elsif value.nil?
        puts challenge.config[name]
      else
        parsed_value = if ((value.to_i.to_s == value) rescue false)
          value.to_i
        elsif ((value.to_f.to_s == value) rescue false)
          value.to_f
        else
          value
        end
        challenge.config[name] = value
        Zll15::Config.save_config(challenge.config.to_hash)
      end
    end


    desc "missions", "dump out list of missions"

    def missions
      challenge = Zll15::Challenge.new()
      challenge.missions.each do |mission|
        puts "%2d %-20s" % [mission.mission_no, mission.title]
      end 
    end

  end

end

