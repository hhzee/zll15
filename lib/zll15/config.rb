
require 'yaml'
require 'pathname'

module Zll15

  class Config

    def Config.config_path( config_path_override )
       if config_path_override 
         config_path_override 
       elsif config_envar = ENV['ZLL15_CONFIG']
         Pathname.new( config_envar )
       else
         default_home + "config.yml"
       end
     end

     def Config.default_home 
       Pathname.new( Dir.home ) + ".zll15" 
     end

     def Config.load_config( config_path_override )
       cp = config_path(config_path_override)
       if cp.exist?
         YAML::load( cp.read  )
       else 
         {}
       end
     end

     def Config.save_config( config_hash, config_path_override )
       old_config = config_path(config_path_override)
       new_config = Pathname.new(new_config_path.to_s + ".tmp")
       new_config.open do |config_file|
         config_file << config_hash.to_yaml
       end
       new_config.rename_to( config_path )
     end


     attr_reader :config_hash

     def initialize( config_hash )
       @config_hash = config_hash
     end

     def home 
       @home ||= self['home.path', Config.default_home]
     end

     def []=( name, value )
       r = @config_hash
       nms = name.split(".")
       nms[0..-2].each do |nm|
         r = (r[nm] ||= {})
       end
       r[nms.last] = value
     end

     def []( name, default = nil )
       r = @config_hash
       nms = name.split(".")
       nms[0..-2].each do |nm|
         r = (r[nm] ||= {})
       end
       r[nms.last] ||= default
     end

     def to_yaml
       @config_hash.to_yaml
     end

  end

end

