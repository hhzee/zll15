
require 'pdf/reader'

module Zll15


    class MissionCmdTarget

      def initialize( challenge, atts = nil )
        atts ||= {}
        @challenge = challenge
        @verbose = atts[:verbose]
        @chars = ""
        @text = []
        @color = ""
        @font_id = nil
        @font_size = nil
        @last_x = nil
        @last_y = nil
        @text_atts = {}
      end

      def set_text_font_and_size( font_id, font_size )
        @font_id = nil
        @font_size = nil
      end

      def set_color( color )
        @color = color
      end

      def append_text( s )
        if @text.empty? || @text.last[:font_id] != @font_id || @text.last[:font_size] != @font_size || @text.last[:color] != @color
          
          @text <<  { font_id: @font_id, font_size: @font_size, color: @color, text: s }
        else
          @text.last[:text] << " #{s}"
        end
      end

      def indent_to( x )
      end

      def outdent_to( x )
      end

      def flush_text
        unless @text.empty?
          if @verbose
            @text.each do |t|
              puts "t=> %6s %2d %06s '%s'" % [
                 t[:font_id] && t[:font_id].to_s || '', 
                 t[:font_size] || 0,  
                 t[:color],
                 t[:text]
              ]
            end
          end
        end
        @text = []
      end

      def flush_chars
        unless @chars.empty? 
          if @verbose
            puts "c=> %5.1f %5.1f %6s %2d %06s '%s'" % [
               @x || -1.0,
               @y || -1.0,
               @font_id && @font_id.to_s || '', 
               @font_size || 0,  
               @color,
               @text
            ]
          end
          if @last_y && @y == @last_y 
            # same line, just append
            puts "?=> append to line" if @verbose
            append_text( @chars )
          else
            if @last_y.nil? || @y > @last_y
              # new page means new paragraph, flush previous, then append
              puts "?=> new page means new paragraph" if @verbose
              flush_text
              append_text( @chars )
              @last_y = @y
              @last_x = @x
            else
              if @last_x && @x == @last_x
                if @last_y - @y < 16
                  # continuation of previous line
                  puts "?=> continued paragraph" if @verbose
                  append_text( @chars )
                else
                  # new paragraph at same level
                  puts "?=> new paragraph" if @verbose
                  flush_text
                  append_text( @chars )
                end
              elsif @last_x && @x < @last_x
                # outdent
                puts "?=> outdent to #{@x}" if @verbose
                flush_text
                outdent_to( @x )
                append_text( @chars )
              elsif @last_x && @x > @last_x
                # indent
                puts "?=> indent to #{@x}" if @verbose
                flush_text
                indent_to( @x )
                append_text( @chars )
              else
                raise "@last_x nil witout @last_y nil -- impossible"
              end
              @last_y = @y
              @last_x = @y
            end
          end
        end
        @chars = ""
      end

      def append_chars( s )
        #puts "==> '#{s}'" if @verbose
        @chars << s
      end

      def start_new_line( x, y )
        @x = x
        @y = y
        puts "==> starting new line at #{x}, #{y}" if @verbose
      end

    end


    class MissionReceiver

      attr_accessor :callbacks

      def initialize( cmd_target )
        @cmd_target = cmd_target
        @callbacks = []
      end


      def respond_to?(meth)
        true
      end

      def show_text(s)
        @cmd_target.append_chars(s)
      end

      def super_show_text(s)
        @cmd_target.append_chars(s)
      end

      def show_text_with_positioning( params )
        params.each do |p|
          case p
          when Float, Fixnum
          else
            @cmd_target.append_chars(p)
          end
        end
      end

      def set_text_font_and_size( font, size )
        @cmd_target.set_text_font_and_size( font, size )
      end

      def move_to_next_line_and_show_text( aw, ac, s )
        @cmd_target.append_chars( s )
      end

      def set_spacing_next_line_show_text( aw, ac, s )
        @cmd_target.append_chars( s )
      end

      def set_text_matrix_and_text_line_matrix( a, b, c, d, x, y )
        @cmd_target.start_new_line( x, y )
      end

      def end_text_object
        @cmd_target.flush_chars
      end
 
      def set_cmyk_color_for_stroking( r, g, b, a ) 
        @cmd_target.set_color( "%02x%02x%02x" % [ (255 * (r || 0)).to_i, (255 * (g || 0)).to_i, (255 * (b || 0)) ] )
      end

      def method_missing(methodname, *args)
        puts "#{methodname} => #{args.inspect}"
      end

    end

  class Challenge

    UPDATES_PDF_URL = "http://www.firstlegoleague.org/sites/default/files/Challenge/TRASH-TREK/FLL%20TRASH%20TREK%20UPDATES.pdf"
    MAIN_PDF_URL = "http://www.firstlegoleague.org/sites/default/files/Challenge/TRASH-TREK/TRASH-TREK-Challenge.pdf"

    def initialize( config_path_override = nil )
      @config = Config.new(Config.load_config(config_path_override))
      @main_pdf_url = @config['challenge.url.main', MAIN_PDF_URL]
      @updates_pdf_url = @config['challenge.url.updates', MAIN_PDF_URL]
      @cache_root = Pathname.new( @config['cache.path', (@config.home + 'cache').to_s] )
      @cache_root.mkpath unless @cache_root.exist?
    end

    def cached_pdf( what, force_load = false )
      cpp = @cache_root + (what.to_s + '.pdf')
      if cpp.exist? && !force_load
        # nothing to do
      else
        url = case what
        when :main
          @main_pdf_url
        when :updates
          @updates_pdf_url
        else
          raise "no url for this pdf: #{what}"
        end
        open( url, 'rb' ) { |remote| cpp.open('wb') { |local| IO.copy_stream( remote, local ) } }
      end
      cpp
    end


    def parse_main

      @rules = []
      @missions = []

      mission_target = MissionCmdTarget.new( self, verbose: true )
      mission_receiver = MissionReceiver.new( mission_target )

      PDF::Reader.open( cached_pdf(:main).to_s ) do |reader|

        reader.pages.map do |page|
          page.walk(mission_receiver)
        end

      end

    end


    def rules
      parse_main unless @rules
      @rules
    end


    def missions 
      parse_main unless @missions
      @missions
    end

  end

end


